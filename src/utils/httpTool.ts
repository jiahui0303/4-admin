import axios from "axios"
import { message } from "ant-design-vue"
import CODEMAP from "./coodMap"
export interface PageNationParams {
  page?: number
  pageSize?: number
}
export interface Params extends PageNationParams {
  [propertyName: string]: string | number | boolean | undefined
}
// console.log(process.env);

const httpTool = axios.create({
  timeout: 10000,
  baseURL: process.env.VUE_APP_BASEURL, //区分环境  开发环境接口（后台，本地）  生成环境接口（线上正式接口）
})

// 添加响应拦截器  对接口返回值做统一处理，比如状态码错误提示，统一包装返回值结果
httpTool.interceptors.response.use(
  (response) => {
    // console.log('interceptors.response:',response);
    if (response.data?.success) {
      //success为true返回data
      return response.data
    }
    message.error(
      response.data?.msg || CODEMAP[response.data?.statusCode || 500]
    )
    return response
  },
  (error) => {
    //非 200~300  非 304
    // console.log(error);
    if (error.code === "ECONNABORTED") {
      // 网络超时
      message.error("您当前网络环境不好，请刷新重试~")
    } else {
      message.error(
        error.response?.statusText ||
          CODEMAP[error.response.data?.status || 400]
      )
    }
    return Promise.reject(error)
  }
)

export default {
  ...httpTool,
  get(url: string, params: Params = {}) {
    // console.log(url, params);
    return httpTool.get(url, {
      params,
    })
  },
  post(url: string, data = {}) {
    return httpTool.post(url, data)
  },
}
